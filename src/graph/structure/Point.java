package graph.structure;

import java.util.ArrayList;
import java.util.List;

public class Point {

	private double x;
	private double y;

	private List<Edge> edges;
	
	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	public Point(double x, double y) {
		this.setX(x);
		this.setY(y);
		this.edges = new ArrayList<Edge>();
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public double distanceToPoint(Point p) {
		return java.lang.Math.sqrt( (p.getX()-x)*(p.getX()-x) + (p.getY()-y)*(p.getY()-y)); 
	}
	
	public void addEdge(Edge edge) {
		this.edges.add(edge);
	}
	
	
	
	public List<Point> getNeighbours() {
		List<Point> neighbours = new ArrayList<Point>();
		for(Edge e : edges) {
			neighbours.add(e.getEndPoint());
		}
		return neighbours;
	}
	
	public String printPoint() {
		return "Point coordinates: x:"+this.getX() + "\t y:" +this.getY() + '\n';
	}
}
