package graph.structure;

import java.util.Comparator;

import heuristics.HeuristicFunction;

public class PointTimeComparator implements Comparator<Point> {

	private Point endpoint;
	private HeuristicFunction heuristic;
	
	public PointTimeComparator(Point endpoint, double maxSpeed){
		this.endpoint = endpoint;
		this.heuristic = new HeuristicFunction(endpoint,maxSpeed);
	}
	
	@Override
	public int compare(Point arg0, Point arg1) {
		double arg0HeuristicValue = heuristic.calculateHeuristicValue(arg0);
		double arg1HeuristicValue = heuristic.calculateHeuristicValue(arg1);
		
		return ((Double) arg0HeuristicValue).compareTo((Double) arg1HeuristicValue);
	}

}