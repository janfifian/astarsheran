package graph.structure;

/**
 * @author Jan Fifian
 * A class representing an edge with one end connected to the point.
 */
public class Edge {

	/**
	 * Ending point of an endge
	 */
	private Point endPoint;
	/**
	 * The total length of the edge in meters/km/whatever
	 */
	private double distance;
	/**
	 * The average speed limit over this edge.
	 */
	private double speedLimit;
	
	
	/**
	 * @param endPoint
	 * @param distance
	 * @param speedLimit
	 * Constructs an edge with a given set of parameters.
	 */
	public Edge(Point endPoint, double distance, double speedLimit) {
		this.endPoint = endPoint;
		this.distance = distance;
		this.speedLimit = speedLimit;
	}
	/**
	 * @return ending point of the edge.
	 */
	public Point getEndPoint() {
		return endPoint;
	}
	/**
	 * @param endPoint 
	 * Sets ending point for the given edge.
	 */
	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
	}
	/**
	 * @return The total length of the edge.
	 */
	public double getDistance() {
		return distance;
	}
	/**
	 * @param distance
	 * Sets the length of the given edge
	 */
	public void setDistance(double distance) {
		this.distance = distance;
	}
	/**
	 * @return 
	 */
	public double getSpeedLimit() {
		return speedLimit;
	}
	/**
	 * @param speedLimit
	 */
	public void setSpeedLimit(double speedLimit) {
		this.speedLimit = speedLimit;
	}
}
