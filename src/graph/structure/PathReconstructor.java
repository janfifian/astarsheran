package graph.structure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class PathReconstructor {

	public static List<Point> staticPathReconstruction(Map<Point,Point> cameFromMap, Point lastPoint){
		List<Point> path = new ArrayList<Point>();
		Point currentPoint = lastPoint;
		while(currentPoint!=null) {
			path.add(currentPoint);
			currentPoint = cameFromMap.get(currentPoint);
		}
		Collections.reverse(path);
		return path;
	}
	
}
