package graph.structure;

import java.util.Comparator;

public class PointDistanceComparator implements Comparator<Point> {

	Point endpoint;
	
	public PointDistanceComparator(Point endpoint){
		this.endpoint = endpoint;
	}
	
	@Override
	public int compare(Point arg0, Point arg1) {
		double arg0ToEndPoint = arg0.distanceToPoint(endpoint);
		double arg1ToEndPoint = arg1.distanceToPoint(endpoint);
		
		return ((Double) arg0ToEndPoint).compareTo((Double) arg1ToEndPoint);
	}

}
