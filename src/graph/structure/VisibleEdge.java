package graph.structure;

import java.util.HashMap;
import java.util.Map;

public class VisibleEdge {
	
	private String edgeEnd1;
	private String edgeEnd2;
	private double distanceInKm;
	private double speedLimit;
	
	public String getEdgeEnd1() {
		return edgeEnd1;
	}
	public void setEdgeEnd1(String edgeEnd1) {
		this.edgeEnd1 = edgeEnd1;
	}
	public String getEdgeEnd2() {
		return edgeEnd2;
	}
	public void setEdgeEnd2(String edgeEnd2) {
		this.edgeEnd2 = edgeEnd2;
	}
	public double getDistanceInKm() {
		return distanceInKm;
	}
	public void setDistanceInKm(double distanceInKm) {
		this.distanceInKm = distanceInKm;
	}
	public double getSpeedLimit() {
		return speedLimit;
	}
	public void setSpeedLimit(double speedLimit) {
		this.speedLimit = speedLimit;
	}
	
	public VisibleEdge(String edgeEnd1, String edgeEnd2, double distanceInKm, double speedLimit) {
		this.edgeEnd1 = edgeEnd1;
		this.edgeEnd2 = edgeEnd2;
		this.distanceInKm = distanceInKm;
		this.speedLimit = speedLimit;
	}
	
	public Edge convertToEdgeForVertices(Map<String, Point> graphPoints) {
		graphPoints.get(edgeEnd1).addEdge(new Edge(graphPoints.get(edgeEnd2),distanceInKm,speedLimit));
		graphPoints.get(edgeEnd2).addEdge(new Edge(graphPoints.get(edgeEnd1),distanceInKm,speedLimit));
		return null;
	}
}
