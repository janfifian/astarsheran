package heuristics;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import graph.structure.Edge;
import graph.structure.Point;
import graph.structure.PointDistanceComparator;
import graph.structure.PointTimeComparator;

public class Astar implements Searcher {
	
	Point endPoint;
	Point startPoint;
	boolean searchByTime;
	HeuristicFunction heur;
	double inversedMaxSpeed;
	
	public Astar(Point endPoint, Point startPoint, boolean searchByTime, HeuristicFunction heuristics, double maxSpeed) {
		this.endPoint = endPoint;
		this.startPoint = startPoint;
		this.searchByTime = searchByTime;
		this.heur = heuristics;
		this.inversedMaxSpeed = 1.0/ maxSpeed;
	}


	@Override
	public Map<Point,Point> determinePathTo() {
		/*
		 * This will hold the result. Each point is mapped onto its predecessor
		 * on path from source.
		 */
		Map<Point,Point> cameFromMap = new HashMap<Point,Point>();
		cameFromMap.put(startPoint, null);
		/*
		 * The set of nodes which need to be explored further.
		 */
		Comparator<Point> comparator = searchByTime? new PointDistanceComparator(endPoint) : new PointTimeComparator(endPoint,1.0/inversedMaxSpeed);
		TreeSet<Point> openSet = new TreeSet<Point>(comparator);
		openSet.add(startPoint);

		/*
		 * gScore map holds the total cost of cheapest path from start node to the given one. 
		 */
		
		Map<Point, Double> gScore = new HashMap<Point, Double>();
		gScore.put(startPoint, (double) 0);
		/*
		 * Then, fScore holds the total cost of cheapest 
		 */
		Map<Point, Double> fScore = new HashMap<Point,Double>();
		fScore.put(startPoint, (double) 0);

		/*
		 * Now, on each iteration consider the point with lowest heuristics.
		 */
		while(!openSet.isEmpty()) {
			Point current = openSet.first();
			if (current == endPoint){
				return cameFromMap;
			}
			openSet.remove(current);
			
			for(Edge edge : current.getEdges() ) {
				Double tentativeGScore = gScore.get(current) + edge.getDistance()*
						(searchByTime? 1.0: 1/edge.getSpeedLimit());
				if (gScore.containsKey(edge.getEndPoint())){
					/*
					 * Rework the map if there is a possible better way...
					 */
					if(gScore.get(edge.getEndPoint())>tentativeGScore){
								
								cameFromMap.put(edge.getEndPoint(), current);
								gScore.put(edge.getEndPoint(), tentativeGScore);
				                fScore.put(edge.getEndPoint(), 
				                		gScore.get(edge.getEndPoint()) + heur.calculateHeuristicValue(edge.getEndPoint()));
				                if (!openSet.contains(edge.getEndPoint())) {
				                	openSet.add(edge.getEndPoint());		
				                }
				         }
					}
				else {
					/*
					 * ...or if there is no way already defined.
					 */
					cameFromMap.put(edge.getEndPoint(), current);
					gScore.put(edge.getEndPoint(), tentativeGScore);
	                fScore.put(edge.getEndPoint(), 
	                		gScore.get(edge.getEndPoint()) + heur.calculateHeuristicValue(edge.getEndPoint()));
	                if (!openSet.contains(edge.getEndPoint())) {
	                	openSet.add(edge.getEndPoint());		
	                }
				}
				
			}
		}
		
		
		/*
		 * Is possible only if there is no path from start to end.
		 */
		return null;
	}

}
