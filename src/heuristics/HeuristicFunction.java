package heuristics;

import graph.structure.Point;

public class HeuristicFunction {
	
	/*
	 * This determines maximum speed
	 * possible for the road in given graph. 
	 * 
	 * When determining the shortest path in the means of time,
	 * 								this should be left as 1.0.
	 */
	double inversedMaxSpeed;

	double startPointX;
	double startPointY;
	double endPointX;
	double endPointY;
	
	public HeuristicFunction(Point endPoint, double maxSpeed) {
		this.startPointX = 0;
		this.startPointY = 0;
		this.endPointX = endPoint.getX();
		this.endPointY = endPoint.getY();
		this.inversedMaxSpeed = 1.0/maxSpeed;
	}
	
	public HeuristicFunction(Point startPoint, Point endPoint, double maxSpeed) {
		this.startPointX=startPoint.getX();
		this.startPointY=startPoint.getY();
		this.endPointX = endPoint.getX();
		this.endPointY = endPoint.getY();
		this.inversedMaxSpeed = 1.0/maxSpeed;
	}
	
	public double calculateHeuristicValue (Point midPoint) {
		return inversedMaxSpeed* java.lang.Math.sqrt( (midPoint.getX() - endPointX)*(midPoint.getX() - endPointX)+ (midPoint.getY() - endPointY)*(midPoint.getY() - endPointY)  );
	}

	public double getInversedMaxSpeed() {
		return inversedMaxSpeed;
	}

	public void setInversedMaxSpeed(double inversedMaxSpeed) {
		this.inversedMaxSpeed = inversedMaxSpeed;
	}

	public double getStartPointX() {
		return startPointX;
	}

	public void setStartPointX(double startPointX) {
		this.startPointX = startPointX;
	}

	public double getStartPointY() {
		return startPointY;
	}

	public void setStartPointY(double startPointY) {
		this.startPointY = startPointY;
	}

	public double getEndPointX() {
		return endPointX;
	}

	public void setEndPointX(double endPointX) {
		this.endPointX = endPointX;
	}

	public double getEndPointY() {
		return endPointY;
	}

	public void setEndPointY(double endPointY) {
		this.endPointY = endPointY;
	}
}
