package heuristics;

import java.util.Map;

import graph.structure.Point;

public interface Searcher {
	
	public Map<Point,Point> determinePathTo();
}
