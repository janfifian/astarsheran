package console.output;

import graph.structure.*;
import heuristics.Astar;
import heuristics.HeuristicFunction;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Console {

	public static void main(String[] args) {
		
			//Input points
		    Scanner scan;
		    Map<String,Point> graphPoints = new HashMap<String,Point>();
		    try {
	        	Pattern p = Pattern.compile("[A-Z]([a-z])*");
			    File pointInputFile = new File("res\\InputPoints.txt");
		        scan = new Scanner(pointInputFile);
		        double x,y;
		        String s;
		        while(scan.hasNextDouble())
		        {
		        	/*
		        	 * Each Point has a format x,y,s,
		        	 * where x and y are its X and Y coordinates
		        	 * and s is the string containing point name in form Abbbbb....b.
		        	 */
		        	x=scan.nextDouble();
		        	y=scan.nextDouble();
		        	s=scan.next(p);
		        	Point P = new Point(x,y);
		        	graphPoints.put(s, P);
		            System.out.print( s ); 
		            System.out.print(":\t\t");
		            System.out.print(P.printPoint());
		        }
		        scan.close();
		    } catch (FileNotFoundException e1) {
		            e1.printStackTrace();
		    }
		
		    List<VisibleEdge> receivedEdges = new ArrayList<VisibleEdge>();
		    //Input edges
		    try {
			    File edgeInputFile = new File("res\\InputEdges.txt");
		        scan = new Scanner(edgeInputFile);
		        while(scan.hasNextLine())
		        {	
		        	String input = scan.nextLine();
		        	String[] inputFrags = input.split(" ");
		        	if(inputFrags.length < 4) {
		        		throw new Exception("Improper input format!");
		        	}
		        	/*
		        	 * Each Edge has a format pointname1,pointname2,distance,speedlimit.
		        	 * The pointnames stand for edge ends.
		        	 * Distance and speedlimit are pretty much selfexplainatory.
		        	 */
		        	VisibleEdge visibleEdge = new VisibleEdge(inputFrags[0],inputFrags[1],Double.parseDouble(inputFrags[2]),Double.parseDouble(inputFrags[3]));
		        	receivedEdges.add(visibleEdge);
		        }	
		        scan.close();
		    } catch (FileNotFoundException e1) {
		            e1.printStackTrace();
		    } catch (Exception e2) {
		    	e2.printStackTrace();
		    }
		
		//Build graph
		for(VisibleEdge visEdge : receivedEdges) {
			visEdge.convertToEdgeForVertices(graphPoints);
		}
		String startPointName = null;
		String endPointName = null;
		double globalSpeedLimit = 0.0;
		Point startPoint = null;
		Point endPoint = null;
		String typeOfPathfinding = "distance";
		//Receive global speedlimit and name of start/end points.
		try {
		    File edgeInputFile = new File("res\\GlobalPresets.txt");
	        scan = new Scanner(edgeInputFile);
	        startPointName = scan.nextLine();
	        endPointName = scan.nextLine();
	        typeOfPathfinding = scan.nextLine();
	        globalSpeedLimit = scan.nextDouble();
	        
	        startPoint = graphPoints.get(startPointName);
	        endPoint = graphPoints.get(endPointName);
	        scan.close();
	        }
	    catch(Exception e) {
	    	e.printStackTrace();
	    }
		
		//Specify heuristic via providing start and endpoint
		HeuristicFunction heuristic = new HeuristicFunction(startPoint,endPoint,globalSpeedLimit);
		System.out.println("Received Input: ");
		System.out.println("EndPoint X: " + heuristic.getEndPointX());
		System.out.println("EndPoint Y: " + heuristic.getEndPointY());
		
		//Execute A*
		Astar Asztar = new Astar(endPoint,startPoint,typeOfPathfinding.equals("time"),heuristic,globalSpeedLimit);
		Map<Point,Point> obtainedAstarSolution = Asztar.determinePathTo();
		//Reconstruct the solution
		
		List<Point> reconstructedPath = PathReconstructor.staticPathReconstruction(obtainedAstarSolution, endPoint);
		try {
			PrintWriter out = new PrintWriter("res\\Output.txt");
			int iterator = 0;
			while(iterator<reconstructedPath.size()) {
				out.println(reconstructedPath.get(iterator).printPoint());
				iterator++;
			}
			out.close();
		} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Process finished.");
	}
}
